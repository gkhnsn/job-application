<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="tr">
<!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="theme-color" content="#0093dd" />
    <link rel='stylesheet' type='text/css' href='//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css'/>
    <link rel='stylesheet' type='text/css' href='//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css'/>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E=" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
</head>
<style>
    .form-control-feedback {
        display: none;
    }

    .has-feedback {
        &.has-success .feedback-success,
        &.has-error .feedback-error {
            display: block;
        }
    }

    .form-group > label + .help-block {
        margin-top: -5px;
        margin-bottom: 5px;
    }
</style>


<?php
    include 'model.php';

    if (isset($_POST['Message'])) {
        echo $_POST['Message'];
    }

    $employee = new Employee();
    $person = new Person();

    $personList = $person->getPeopleList();
    $employeeList = $employee->getEmployeeList();
?>

<body class="page-header-fixed page-quick-sidebar-over-content">
    <!-- <h2>Tarfin Apply Job</h2> -->
    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#defaultNavbar1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
          <img src="assets/img/tarfin.png" alt="Tarfin logo" width="200" class="img-responsive header-logo" style="width: 70px;"></div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="defaultNavbar1">

          <ul class="nav navbar-nav navbar-right">
            <li><a href="index.php">Home</a></li>
            <li><a href="addPerson.php">Add Person</a></li>
          </ul>
        </div>
        <!-- /.navbar-collapse -->
      </div>
      <!-- /.container-fluid -->
    </nav>
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <div class="table-responsive">
            <table summary="This table shows how to create responsive tables using Bootstrap's default functionality" class="table table-bordered table-hover">
              <caption class="text-center">People Table</caption>
              <thead>
                <tr>
                  <th>First Name</th>
                  <th>Last Name</th>
                  <th>Identity Number</th>
                  <th>Birth Year</th>
                  <th>Last Application Date</th>
                  <th>Application Count</th>
                </tr>
              </thead>
              <tbody>
                <?php
                    if (count($personList) > 0) {
                        foreach ($personList as $key => $value) {
                            $link = ($value['employment_status'] === 'Passive') ? ' Add' : '';
                            echo '<tr>';
                                echo '<td>'.$value['first_name'].'<a href=updateEmploymentStatus.php?status=Active&id='.$value['id'].'>'.$link.'</a></td>';
                                echo '<td>'.$value['last_name'].'</td>';
                                echo '<td>'.$value['identity_number'].'</td>';
                                echo '<td>'.$value['birth_year'].'</td>';
                                echo '<td>'.$value['last_application_date'].'</td>';
                                echo '<td>'.$value['application_count'].'</td>';
                            echo '</tr>';
                        }
                    } else {
                        echo '<tr>';
                            echo '<td colspan="6" class="text-center text-danger">No data found</td>';
                        echo '</tr>';
                    }
                ?>
              </tbody>
              <!-- <tfoot>
                <tr>
                  <td colspan="7" class="text-center">Data retrieved from <a href="http://www.infoplease.com/ipa/A0855611.html" target="_blank">infoplease</a> and <a href="http://www.worldometers.info/world-population/population-by-country/" target="_blank">worldometers</a>.</td>
                </tr>
              </tfoot> -->
            </table>
          </div><!--end of .table-responsive-->
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <div class="table-responsive">
            <table summary="This table shows how to create responsive tables using Bootstrap's default functionality" class="table table-bordered table-hover">
              <caption class="text-center">Employee Table</caption>
              <thead>
                <tr>
                  <th>Person ID</th>
                  <th>Employee Name</th>
                  <th>Employment Start Date</th>
                  <th>Position Name</th>
                  <th>Employment Status</th>
                </tr>
              </thead>
              <tbody>
                <?php
                    if (count($employeeList) > 0) {
                        foreach ($employeeList as $key => $value) {
                            $link = ($value['employment_status'] === 'Active') ? ' Remove' : '';
                            echo '<tr>';
                                echo '<td>'.$value['person_id'].'</td>';
                                echo '<td>'.$value['first_name'].' '.$value['last_name'].' <a href=updateEmploymentStatus.php?status=Passive&id='.$value['person_id'].'>'.$link.'</a></td>';
                                echo '<td>'.$value['employment_start_date'].'</a></td>';
                                echo '<td>'.$employee->positionNames[$value['position_id']].'</td>';
                                echo '<td>'.$value['employment_status'].'</td>';
                            echo '</tr>';
                        }
                    } else {
                        echo '<tr>';
                            echo '<td colspan="6" class="text-center text-danger">No data found</td>';
                        echo '</tr>';
                    }
                ?>
              <!-- <tfoot>
                <tr>
                  <td colspan="5" class="text-center">Data retrieved from <a href="http://www.infoplease.com/ipa/A0855611.html" target="_blank">infoplease</a> and <a href="http://www.worldometers.info/world-population/population-by-country/" target="_blank">worldometers</a>.</td>
                </tr>
              </tfoot> -->
            </table>
          </div>
        </div>
      </div>
    </div>
</body>
</html>







