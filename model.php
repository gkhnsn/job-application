<?php
    include 'config/database.php';

    class DbConnection {
		public $config;
		public $con;

    	public function __construct() {
    		$this->config = new DATABASE_CONFIG();
    		$this->con = mysqli_connect($this->config->HOST_MYSQL, $this->config->DB_USERNAME_MYSQL, $this->config->DB_PASSWORD_MYSQL, 'job_application', 3306)
				OR die(mysqli_error());

			if (!$this->con) {
			    echo "Error: Unable to connect to MySQL." . PHP_EOL;
			    echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
			    echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
			    exit;
			}

			/* change character set to utf8 */
			if (!$this->con->set_charset("utf8")) {
			    printf("Error loading character set utf8: %s\n", $this->con->error);
			    exit();
			}
    	}
    }

    class Person {
        private $id;
        public $personID;
        public $firstName;
        public $lastName;
        public $idendtityNumber;
        public $birthYear;
        public $applicationCount;
        public $lastApplicationDate;
        public $jobID;
        private $connection;

        public function __construct() {
        	$this->connection = new DbConnection();

        	$employee = new Employee();
        }

        private function updatePeople () {
        	$dateTime = new DateTime();
        	$stmt = $this->connection->con->prepare("UPDATE People SET last_application_date = ?, application_count = application_count + 1 WHERE identity_number = ?");

			$stmt->bind_param('ss', $dateTime->format('Y-m-d H:i:s'), $this->idendtityNumber);

			if(!$stmt->execute()){
	            print_r("Error : $conn->error");
	       	}

	       	$stmt->close();

			header('Location: index.php');
    	}

        public function savePeople () {
			try {
				$existUser = $this->getPeople();

				if (count($existUser) > 0) {
					$this->updatePeople();
				} else {
					$stmt = $this->connection->con->prepare("INSERT INTO People (first_name, last_name, birth_year, identity_number, last_application_date, application_count)
						VALUES (?, ?, ?, ?, ?, ?)");

					$stmt->bind_param('ssiisi', $this->firstName, $this->lastName, $this->birthYear, $this->idendtityNumber, $this->lastApplicationDate, $this->applicationCount);

					if(!$stmt->execute()){
			            print_r("Error : $conn->error");
			       }

					$stmt->close();

					// Get last generated id
					$sql = "SELECT LAST_INSERT_ID()";
					$result = $this->connection->con->query($sql);

				    while($row = $result->fetch_assoc()) {
						$this->personID = (int) $row['LAST_INSERT_ID()'];
				    }

				    $this->saveEmployee();

        			header('Location: index.php');
				}
		    } catch (Exception $exc) {
		    	echo $exc->getMessage();
			}
        }

        public function saveEmployee () {
        	$dateTime = new DateTime();
        	$employmentStartDate = $dateTime->format('Y-m-d H:i:s');
        	$status = 'Passive';

        	$stmt = $this->connection->con->prepare("INSERT INTO Employee (person_id, employment_start_date, position_id, employment_status)
				VALUES (?, ?, ?, ?)");

			$stmt->bind_param('isis', $this->personID, $employmentStartDate, $this->jobID, $status);

			if(!$stmt->execute()){
	            print_r("Error : $conn->error");
	       }

	       $stmt->close();
        }

        public function getPeopleList () {
			try {
				$stmt = $this->connection->con->prepare("SELECT p.*, e.employment_status FROM People as p INNER JOIN Employee as e ON p.id = e.person_id");

				$stmt->execute();

				$result = $stmt->get_result();

				$res_arr_values = array();
				while ($row = $result->fetch_assoc()) {
				    array_push($res_arr_values, $row);
				}

				$stmt->close();

				return $res_arr_values;
		    } catch (Exception $exc) {
		    	echo $exc->getMessage();
			}
        }

        private function getPeople () {
			try {
				$stmt = $this->connection->con->prepare("SELECT * FROM People WHERE identity_number = ?");
				$stmt->bind_param('s', $this->idendtityNumber);

				$stmt->execute();

				$result = $stmt->get_result();

				$res_arr_values = array();
				while ($row = $result->fetch_assoc()) {
				    array_push($res_arr_values, $row);
				}

				$stmt->close();

				return $res_arr_values;
		    } catch (Exception $exc) {
		    	echo $exc->getMessage();
			}
        }
    }

    class Employee extends Person {
        private $id;
        public $personID;
        public $employmentStartDate;
        public $positionName;
        public $employmentStatus;
        private $connection;
        public $status = 'Passive';

        public $positionNames = [
            1 => 'Software Developer',
            2 => 'Frontend Developer',
            3 => 'Backend Developer',
            4 => 'Fullstack Developer',
            5 => 'Physics Teacher',
            6 => 'Math Teacher',
            7 => 'Farmer',
            8 => 'Finance Manager'
        ];

        public function __construct() {
        	$this->connection = new DbConnection();
        }

        public function getEmployeeList () {
			try {
				$stmt = $this->connection->con->prepare("SELECT e.*, p.first_name , p.last_name FROM People as p INNER JOIN Employee as e ON p.id = e.person_id");

				$stmt->execute();

				$result = $stmt->get_result();

				$res_arr_values = array();
				while ($row = $result->fetch_assoc()) {
				    array_push($res_arr_values, $row);
				}

				$stmt->close();

				return $res_arr_values;
		    } catch (Exception $exc) {
		    	echo $exc->getMessage();
			}
        }

       	public function updateEmploymentStatus () {
        	$dateTime = new DateTime();
        	$stmt = $this->connection->con->prepare("UPDATE Employee SET employment_status = ?, employment_start_date = ? WHERE person_id = ?");

			$stmt->bind_param('ssi', $this->employmentStatus, $dateTime->format('Y-m-d H:i:s'), $this->personID);

			if(!$stmt->execute()){
	            print_r("Error : $conn->error");
	       	}

	       	$stmt->close();

			header('Location: index.php');
       	}
    }