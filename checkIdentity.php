<?php
if (!(isset($_POST) && isset($_POST['firstname']) && isset($_POST['lastname']) && isset($_POST['birthyear']) && isset($_POST['identitynumber']))) {
    return false;
}

include 'model.php';

try {
    $person = new Person();

    $firstName = mb_strtoupper($_POST['firstname']);
    $lastName = mb_strtoupper($_POST['lastname']);
    $birthYear = intval($_POST['birthyear']);
    $idendtityNumber = (int) $_POST['identitynumber'];
    $jobID = (int) $_POST['jobid'];

    $request = new SoapClient('https://tckimlik.nvi.gov.tr/Service/KPSPublic.asmx?WSDL');

    $result = $request->TCKimlikNoDogrula(array(
        'TCKimlikNo' => $idendtityNumber,
        'Ad' => $firstName,
        'Soyad' => $lastName,
        'DogumYili' => $birthYear)
    );

    if ($result->TCKimlikNoDogrulaResult) {
        $response = true;

        $dateTime = new DateTime();

        $person->firstName = $firstName;
        $person->lastName = $lastName;
        $person->birthYear = $birthYear;
        $person->idendtityNumber = $idendtityNumber;
        $person->jobID = $jobID;
        $person->lastApplicationDate = $dateTime->format('Y-m-d H:i:s');
        $person->applicationCount = 1;
        $person->savePeople();
    } else {
        $response = false;

        header("Status: 204 Moved Permanently");
        header('Location: addPerson.php');
    }
} catch (Exception $exc) {
    echo $exc->getMessage();
}
