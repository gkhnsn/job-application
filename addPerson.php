<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="tr">
<!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="theme-color" content="#0093dd" />
    <link rel='stylesheet' type='text/css' href='//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css'/>
    <link rel='stylesheet' type='text/css' href='//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css'/>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E=" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="//cdn.jsdelivr.net/jquery.validation/1.15.0/jquery.validate.min.js"></script>
    <!-- <script src="https://requirejs.org/docs/release/2.3.5/minified/require.js"></script> -->
</head>
<style>
    .form-control-feedback {
        display: none;
    }

    .has-feedback {
        &.has-success .feedback-success,
        &.has-error .feedback-error {
            display: block;
        }
    }

    .form-group > label + .help-block {
        margin-top: -5px;
        margin-bottom: 5px;
    }

    .error {
        color: #e74c3c;
    }
</style>

<?php
    include 'model.php';

    $employee = new Employee();
?>

<body class="page-header-fixed page-quick-sidebar-over-content">
    <!-- <h2>Tarfin Apply Job</h2> -->
    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#defaultNavbar1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
          <img src="assets/img/tarfin.png" alt="Tarfin logo" width="200" class="img-responsive header-logo" style="width: 70px;"></div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="defaultNavbar1">

          <ul class="nav navbar-nav navbar-right">
            <li><a href="index.php">Home</a></li>
            <li><a href="addPerson.php">Add Person</a></li>
          </ul>
        </div>
        <!-- /.navbar-collapse -->
      </div>
      <!-- /.container-fluid -->
    </nav>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1>Job Application Form</h1>
            </div>
        </div>
    </div>
    <form id="job-apply" method="post" action="checkIdentity.php">
        <div class="container">
            <div class="row">
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="first-name">First Name</label>
                        <div class="form-control-wrapper">
                            <input type="text" class="form-control" id="first-name" name="firstname" autocomplete="off">
                            <span class="fa fa-check form-control-feedback feedback-success" aria-hidden="true"></span>
                            <span class="fa fa-times form-control-feedback feedback-error" aria-hidden="true"></span>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="last-name">Last Name</label>
                        <div class="form-control-wrapper has-feedback">
                            <input type="text" class="form-control" id="last-name" name="lastname" autocomplete="off">
                            <span class="fa fa-check form-control-feedback feedback-success" aria-hidden="true"></span>
                            <span class="fa fa-times form-control-feedback feedback-error" aria-hidden="true"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="birth-year">Birth Year</label>
                        <!-- <span class="help-block">Lorem ipsum dolor sit amet.</span> -->
                        <div class="form-control-wrapper">
                            <input type="text" class="form-control" id="birth-year" name="birthyear" autocomplete="off">
                            <span class="fa fa-check form-control-feedback feedback-success" aria-hidden="true"></span>
                            <span class="fa fa-times form-control-feedback feedback-error" aria-hidden="true"></span>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="identity-number">Identity Number</label>
                        <!-- <span class="help-block">Lorem ipsum dolor sit amet.</span> -->
                        <div class="form-control-wrapper has-feedback">
                            <input type="text" class="form-control" id="identity-number" name="identitynumber" autocomplete="off">
                            <span class="fa fa-check form-control-feedback feedback-success" aria-hidden="true"></span>
                            <span class="fa fa-times form-control-feedback feedback-error" aria-hidden="true"></span>
                            <!-- <span class="help-block text-warning">Lorem ipsum dolor sit amet!</span> -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="job-position">Job Position</label>
                        <!-- <span class="help-block">Lorem ipsum dolor sit amet.</span> -->
                        <div class="form-control-wrapper">
                            <div class="form-group">
                                <select class="form-control" id="job-id" name="jobid">
                                    <?php
                                        foreach ($employee->positionNames as $key => $value) {
                                            echo "<option value='$key'>$value</option>";
                                        }
                                    ?>
                                </select>
                             </div>
                            <span class="fa fa-check form-control-feedback feedback-success" aria-hidden="true"></span>
                            <span class="fa fa-times form-control-feedback feedback-error" aria-hidden="true"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <button type="submit" id="submit-button" class="btn btn-primary">Send</button>
                </div>
            </div>
        </div>
    </form>
</body>

<script type="text/javascript">
    $(document).ready(function () {
        $.validator.addMethod("letters", function(value, element) {
            return this.optional(element) || value == value.match(/^[a-zA-ZğüşöçıİĞÜŞÖÇ\s]*$/);
        });

        $.validator.addMethod("identity", function(value, element) {
            var tckontrol,toplam;
            tcno = value;
            toplam = Number(tcno.substring(0,1)) + Number(tcno.substring(1,2)) + Number(tcno.substring(2,3)) + Number(tcno.substring(3,4)) + Number(tcno.substring(4,5)) + Number(tcno.substring(5,6)) + Number(tcno.substring(6,7)) + Number(tcno.substring(7,8)) + Number(tcno.substring(8,9)) + Number(tcno.substring(9,10));

            strtoplam = String(toplam);

            onunbirlerbas = strtoplam.substring(strtoplam.length, strtoplam.length-1);

            $result = false;
            if(onunbirlerbas == tcno.substring(10,11)) {
                $result = true;
            }

            return $result;
        });

        $("#job-apply").validate({
            rules: {
                firstname: {
                    required: true,
                    minlength: 3,
                    letters: true
                },
                lastname: {
                    required: true,
                    minlength: 3,
                    letters: true
                },
                birthyear: {
                    number: true,
                    required: true
                },
                identitynumber: {
                    number: true,
                    minlength: 11,
                    identity: true
                },
                jobposition: {
                    required: true,
                    letters: true
                }
            },
            messages: {
                firstname: "Please specify your name (only letters and spaces are allowed)",
                lastname: "Please specify your last name (only letters and spaces are allowed)",
                birthyear: "Please specify your birth year",
                identitynumber: "Identity number is not valid",
                jobposition: "Please select job positin",
                email: "Please specify a valid email address",
            },
        });


    });
</script>
</html>