<?php
if (!(isset($_GET) && isset($_GET['status']) && isset($_GET['id']))) {
    return false;
}

include 'model.php';

try {
    $employee = new Employee();
    $employee->personID = (int) $_GET['id'];
    $employee->employmentStatus = $_GET['status'];

    $employee->updateEmploymentStatus();
} catch (Exception $exc) {
    echo $exc->getMessage();
}